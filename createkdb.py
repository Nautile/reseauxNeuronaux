'''
this file create a knowledge database (kdb)
this database is store into the folder : kdb and into the affine.txt file
'''
from random import randint
import os
from pathlib import Path
import math

# Mode openning file
modeOpenFile = "a+"
# file use to store
fileAffine = "kdb/affine.txt"

# Define number of point created
number = 1000

if Path( fileAffine ).is_file():
	os.remove( fileAffine )

# Define the max X
minRange = number * (-1)
maxRange = number

# list contain all x use
listXUse = []

# Set a affine function
# Ask A
a = input("Please enter a : ")
a = int(a)

# Ask B
b = input("Please enter b : ")
b = int(b)

i = 0

# generate all points
while len(listXUse) < number :
	x = randint(minRange, maxRange)

	if x not in listXUse :
		listXUse.append( x )
		y = a*x+b

		toWrite = ""
		if i != 0 :
			toWrite += "\n"

		toWrite += "("+str(x)+","+str(y)+")"

		# Write into file
		fichier = open( fileAffine, modeOpenFile )
		fichier.write( toWrite )
		fichier.close()

	i += 1

print("done !")